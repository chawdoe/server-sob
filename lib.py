import json
import socket
import os
import time
import sys
from PIL import Image

def infoToJson(data):
    content = json.dumps(data)
    #print("after dumps:",content)
    return content

def jsonToBinary(content):
    #print("after 2:",content.encode("utf-8"))
    return content.encode("utf-8")

def toBinary(content):
    return jsonToBinary(infoToJson(content))

def binaryToJson(content):
    #print("decode 2:",content.decode('utf-8'))
    return content.decode("utf-8")

def jsonToInfo(content):
    #print("after loads:",json.loads(content))
    data = json.loads(content)
    return data

def toInfo(content):
    return jsonToInfo(binaryToJson(content))

def send(sk,theData):
    sk.sendall(toBinary(theData))
    return True

def recv(sk):
    data = ""
    while 1:
        temp = sk.recv(1024)
        if not temp or len(temp)==0:
            #print('reach the end of datas')
            break
        else:
            data += binaryToJson(temp)
            if len(data)<1024:
                if data[-3:] =="end":
                    new_data = data[:-3]
                    data = new_data
                break
            elif data[-3:] == "end":
                #print(data)
                new_data = data[:-3]
                return jsonToInfo(new_data)
    #print("before toInfo:", data)#data is in order

    if not data:
        return False
    return jsonToInfo(data)
    #return toInfo(temp)

def receiveMessage(sk):
    temp = sk.recv(10240)
    time.sleep(1)
    if not temp:
        return False
    return temp.decode('utf-8')

def creatSendInfo(username, password, is_login = False):
    the_info = {"username": "", "password": "", "is_login": False}
    the_info["username"] = username
    the_info["password"] = password
    the_info["is_login"] = is_login
    return the_info

def createSocketConnection(the_ip = "139.199.179.136",the_port = 80):
    sk = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        sk.connect((the_ip, the_port))
        return sk
    except OSError:
        return False

def SaveOnline(username,file,who):
    sk = createSocketConnection()
    if sk == False:
        return False
    flag = "save" + file
    content = json.dumps(who)
    dict_info = {"username":username,"flag":flag,"content":content}
    end_info = "end"
    str_info = json.dumps(dict_info)
    my_info = str_info+end_info
    b_my_info = jsonToBinary(my_info)
    try:
        sk.sendall(b_my_info)
        recv_message = recv(sk)
        sk.close()
        return recv_message
    except OSError:
        return False

def LoadOnline(username,file,who):
    sk = createSocketConnection()
    if sk == False:
        return False
    flag = "load" + file
    dict_info = {"username":username,"flag":flag,"content":""}
    try:
        send(sk,dict_info)
        temp = recv(sk)
        recv_info = temp
        sk.close()
    except OSError:
        return False
    remp = {ord('\\'):None}
    temp_str = json.dumps(recv_info)
        # so make it normal.
    a = temp_str.translate(remp)
    json_info = a[3:-3]
    if json_info == None or len(json_info)<4:
        json_info = "{}"
    data = json.loads(json_info)
    who.clear()
    who.update(data)
    return True

def closeConnection():
    sk = createSocketConnection()
    if sk == False:
        return False
    dict_info = {"close": True}
    send(sk,dict_info)
    recv_info = receiveMessage(sk)
    sk.close()
    return recv_info

def sendBig(sk,theData):
    end_info = "end"
    str_info = json.dumps(theData)
    my_info = str_info+end_info
    b_my_info = jsonToBinary(my_info)
    try:
        sk.sendall(b_my_info)
        return True
    except OSError:
        return False

def createUserPicsFile(username):
    mypath = sys.path[0]
    print("mypath:" + mypath)
    newpath = mypath +"pics/"+ username
    if(os.path.exists(newpath)):
        return False
    try:
        os.mkdir("pics/"+username)
        return True
    except OSError:
        return False

def loadPicsOnline(username,who):
    sk = createSocketConnection()
    if sk == False:
        return False

    send_dict = {"username":username,"who":who,"action":"get"}
    send_info = infoToJson(send_dict) + "end"
    b_send_info = jsonToBinary(send_info)
    try:
        sk.sendall(b_send_info)
        temp = recv(sk)

        buffer = temp["content"]
        width = temp["width"]
        height = temp["height"]
        sk.close()

        return buffer,width,height
    except OSError:
        return False

def savePicsOnline(username,path,who):

    sk = createSocketConnection()
    if sk == False:
        return False

    try:
        img = Image.open(path[0])
        buffer = img.convert('RGBA').tobytes()
        width = img.size[0]
        height = img.size[1]
        send_dict={"username":username,"content":buffer,"width":width,"height":height,"who":who,"action":"post"}
        send_info = infoToJson(send_dict) + "end"
        b_send_info = jsonToBinary(send_info)
        try:
            sk.sendall(b_send_info)
            recv_info  = receiveMessage(sk)
            sk.close()
            return recv_info
        except OSError:
            return False
    except OSError:
        return False
