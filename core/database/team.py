from core.database.base import Base


class Team(Base):
    def add(self, key):
        Base.add(self, key)

        self[key]["list"] = []
        self[key]["score"] = [0, 0, 0, 0, 0]

    def check_member(self, key, name):
        return name in self[key]["list"]

    def rename_member(self, key, old, new):
        if old == new or self.check_member(key, new) is True:
            return False

        lst = self[key]["list"]

        for n in range(0, len(lst)):
            if lst[n] == old:
                lst[n] = new
                break

        return True

    def start_game(self, key):
        self[key]["score"] = [0, 0, 0, 0, 0]

    def add_score(self, key, index, value):
        score = self[key]["score"]
        score[index] += value
        score[4] = 0

        for n in range(0, 4):
            score[4] += score[n]  # 4 is total
