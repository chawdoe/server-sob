from core.database.team import Team
from core.database.member import Member
import collections
import os
from Mylib import pics_lib
import json
import pickle

#the flag == "player" or "team"
class User(collections.OrderedDict):
    def add_user(self, key):
        member = Member()
        team = Team()
        self[key] = {
            "player": member,
            "team": team
        }

    def get_all_list(self, username, flag):#get the list of all players or teams. return list
        __list = []
        for i in self[username][flag]:
            __list.append(i)
        return __list

    def get_one_dict(self, username, flag, target):#get the info(a dict)of the player or team.
        #return a dict.
        return self[username][flag].get(target)


    def add_one(self, username, flag):#add a player or team,         #retrun T/F.
        _count = 0

        while self[username][flag].check(flag + str(_count)):
            _count += 1
        target = flag + str(_count)

        return self[username][flag].add(target)

    def modify_one(self, username, flag, recv_dict):#update the player or the team
        self[username][flag][recv_dict["target"]].update(recv_dict["content"])

    def start_game(self, username, team_key):
        self[username]["team"].start_game(team_key)

        for i in  self[username]["team"][team_key]["list"]:
            self[username]["player"].start_game(i)

    def rename_team(self, username, old, new):
        is_successful = self[username]["team"].rename(old, new)

        if is_successful:
            self.rename_pic(username, "team", old, new)
            for i in self[username]["team"][new]["list"]:
                self[username]["player"][i]["team_name"] = new

        return is_successful

    def rename_member(self, username, old, new):
        is_successful = self[username]["player"].rename(old, new)

        if is_successful:
            self.rename_pic(username, "player", old, new)
            team_name = self[username]["player"][new]["team_name"]

            if team_name != "":
                return self[username]["team"].rename_member(team_name, old, new)

        return is_successful

    def del_team(self, username, key):
        for i in self[username]["team"][key]["list"]:
            self[username]["player"][i]["team_name"] = ""

        self[username]["team"].pop(key)
        self.del_pic(username, "team", key)

    def del_member(self, username, key):
        team_name = self[username]["player"][key]["team_name"]
        self[username]["player"].pop(key)
        self.del_pic(username, "player", key)
        if team_name != "":
            self.del_team_member(username, team_name, key)

    def del_team_member(self, username, key, name):
        lst = self[username]["team"][key]["list"]

        for i in lst:
            if i == name:
                if self[username]["player"].check(name):
                    self[username]["player"][name]["team_name"] = ""
                break

        lst.remove(i)
        return True

    def add_team_member(self, username, team_name, player_name):
        for i in self[username]["team"] :
            if self[username]["team"].check_member(i, player_name):
                return False

        for i in self[username]["team"][team_name]["list"]:
            if i == player_name:
                return False

        self[username]["team"][team_name]["list"].append(player_name)
        self[username]["player"][player_name]["team_name"] = team_name
        return True

    def get_user(self, username):
        if self.check_user(username) is True:
            return self[username]
        return None

    def check_user(self, username):
        return username in self.keys()

    def update_user(self, username, player_dict, team_dict):
        if self.check_user(username) is True:
            if player_dict != {}:
                self[username]["player"].update(player_dict)
            if team_dict != {}:
                self[username]["team"].update(team_dict)
            return True
        return False

    def start_team_game(self, username, team_a, team_b):
        for i in self[username]["team"][team_a]["list"]:
            self[username]["player"].start_game(i)
        for i in self[username]["team"][team_b]["list"]:
            self[username]["player"].start_game(i)

    def set_pics(self, username, flag, name, buffer):##flag = "player" or flag = "team", buffer is the b64 buffer.
        if not os.path.exists("pics"):
            os.mkdir("pics")
        pathname = "./pics/" + username
        if not os.path.exists(pathname):
            os.mkdir(pathname)
        new_dir = pathname + "/" + flag
        if not os.path.exists(new_dir):
            os.mkdir(new_dir)
        path = new_dir + "/" + name + ".png"
        if buffer != None and buffer != False:
            self[username][flag].set_picture(name, path, buffer)
            return path
        return ""

    def get_pic_buffer(self, username, flag, name):##return a b64 buffer
        if not os.path.exists("pics"):
            os.mkdir("pics")
        pathname = "./pics/" + username
        if not os.path.exists(pathname):
            return ""
        new_dir = pathname + "/" + flag
        if not os.path.exists(new_dir):
            return ""
        path = new_dir + "/" + name + ".png"
        if os.path.isfile(path):
            return pics_lib.picsToB64(path)
        return ""

    def rename_pic(self, username, flag, name, newname):
        if not os.path.exists("pics"):
            os.mkdir("pics")
        pathname = "./pics/" + username
        if not os.path.exists(pathname):
            return False
        new_dir = pathname + "/" + flag
        if not os.path.exists(new_dir):
            return False
        path = new_dir + "/" + name + ".png"
        newpath = new_dir + "/" + newname + ".png"
        if os.path.isfile(path):
            os.rename(path, newpath)
            self[username][flag].rename_picture(name, newpath)
        return True

    def del_pic(self, username, flag, name):
        if not os.path.exists("pics"):
            os.mkdir("pics")
        pathname = "./pics/" + username
        if not os.path.exists(pathname):
            return False
        new_dir = pathname + "/" + flag
        if not os.path.exists(new_dir):
            return False
        path = new_dir + "/" + name + ".png"
        if os.path.isfile(path):
            os.remove(path)
            self[username][flag].del_picture(name)
        return True

    def save_user(self, username):
        f = open('./data/' + username + '.json', "wb+")
        if username in self:
            content = pickle.dumps(self[username])
            f.write(content)
        f.close()

    def save_all(self):
        for i in self:
            self.save_user(i)

    def check(self, username, password):
        f1 = open("database.txt")
        fw1 = f1.read()
        database = json.loads(fw1)
        f1.close()
        if username in database:
            if database[username] == password:
                return True
        return False

    def login(self, username, password):
        f1 = open("database.txt")
        fw1 = f1.read()
        database = json.loads(fw1)
        f1.close()
        if username in database:
            if database[username] == password:
                filename = "./data/" + username + '.json'
                f = open(filename, "rb")
                fw = f.read()
                f.close()
                self[username] = pickle.loads(fw)
                return pickle.loads(fw)
        return None

    def register(self, username, password):
        if not os.path.exists("data"):
            os.mkdir("data")
        if not os.path.exists("database.txt"):
            f1 = open("database.txt", "w+")
            f1.write("{}")
            f1.close()
        f1 = open("database.txt")
        fw1 = f1.read()
        database = json.loads(fw1)
        f1.close()

        if username in database:
            return False
        else:
            database[username] = password

        f2 = open("database.txt", "w+")
        f2.write(json.dumps(database))
        f2.close()

        f = open("./data/" + username + '.json', "wb+")
        self.add_user(username)
        f.write(pickle.dumps(self[username]))
        f.close()

        return True

    def load(self, username):
        f1 = open("database.txt")
        fw1 = f1.read()
        database = json.loads(fw1)
        f1.close()
        if username in database:
            filename = "./data/" + username + '.json'
            f = open(filename, "rb")
            fw = f.read()
            f.close()
            my_data = pickle.loads(fw)
            self[username] = my_data
            return True
        return False