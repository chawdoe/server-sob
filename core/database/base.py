from Mylib import pics_lib
import collections


class Base(collections.OrderedDict):
    def add(self, key):
        self[key] = {
            "picture": {
                "path": ""  # In the server which place store the picture.
            },
            "name": key
        }

    def check(self, key):
        return key in self.keys()

    def get(self, key):
        existing = self.check(key)

        if existing is False:
            return None
        return self[key]

    def rename(self, old, new):
        if old == new or self.check(new) is True:
            return False

        tmp = self[old]
        self[new] = tmp
        self.pop(old)
        self[new]["name"] = new

        return True

    def set_picture(self, key, path, buffer):
        #print(path)
        pics_lib.b64ToPics(buffer, path)##this save the b64 buffer at the path.
        self[key]["picture"] = {
            "path": path
        }

    def rename_picture(self, key, path):
        if self.check(key) is True:
            self[key]["picture"]["path"] = path
            return True
        return False

    def del_picture(self, key):
        if self.check(key) is True:
            self[key]["picture"]["path"] = ""
            return True
        return False

