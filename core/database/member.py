from core.database.base import Base


class Member(Base):
    def add(self, key):
        Base.add(self, key)

        data = {
            "age": 0,
            "height": 0,
            "weight": 0,
            "number": 0,
            "type": "PG",
            "team_name": "",
            "game_times": 0,
            "all_data": {
                "pts": 0,  # points
                "rebs": 0,
                "asts": 0,
                "tovs": 0,
                "stls": 0,
                "blks": 0,
                "fouls": 0,
                "3PA": 0,
                "3P": 0,
                "2PA": 0,
                "2P": 0,
                "FGA": 0,
                "FG": 0
            },
            "data": {
                "pts": 0,  # points
                "rebs": 0,
                "asts": 0,
                "tovs": 0,
                "stls": 0,
                "blks": 0,
                "fouls": 0,
                "3PA": 0,
                "3P": 0,
                "2PA": 0,
                "2P": 0,
                "FGA": 0,
                "FG": 0
            }
        }

        self[key].update(data)

    def start_game(self, key):
        self[key]["game_times"] += 1
        self[key]["data"] = {
            "pts": 0,  # points
            "rebs": 0,
            "asts": 0,
            "tovs": 0,
            "stls": 0,
            "blks": 0,
            "fouls": 0,
            "3PA": 0,
            "3P": 0,
            "2PA": 0,
            "2P": 0,
            "FGA": 0,
            "FG": 0
        }
