import socketserver
import Mylib.lib
import socket
import sql
import Mylib.server_lib
from threading import Thread
import core
import time
import sys


# local_ip = "10.104.173.161"
# local_port = 8080


local_port = 8080
ADDR = ('', local_port)


def dealwithLoad(username, bcd):##when the user login, load the user data
    #it seems that must have the second Argument.

    core.user.load(username)


def dealwithSleep():#the server save the data in the database every one minute.
    while 1:
        time.sleep(1)
        for i in core.user:
            try:
                core.user.save_user(i)
            except OSError:
                print("save error")
                break


def get_ip_from_index(index):##获取内网IP, 0表示本地 1表示广州服务器 2表示新加坡服务器
    if index == 0:
        return ''
    elif index == "1":#guangzhou
        return '10.104.173.161'
    elif index == "2":
        return '10.130.51.174'



class MyTcpHandler(socketserver.BaseRequestHandler):

    def handle(self):

        data = Mylib.lib.recv(self.request)
        print("listen success!")

        if data == False:
            self.request.close()
            return
        conn = self.request
        print(data)
        if "is_login" in data.keys():
            if data["is_login"] == True:
                print("The client ask for logging in...")
                data.pop("is_login")
                account_data = core.user.check(data["username"],data["password"])
                print(core.user)
                if account_data and not data["username"] in core.user:
                    core.user.add_user(data["username"])
                    print(data["username"]+" login successfully")
                    load_thread = Thread(target = dealwithLoad, args=(data["username"], data["password"]))
                    load_thread.start()
                    conn.sendall('login successfully'.encode("utf-8"))

                elif account_data == False:
                    print("the account does not match!")
                    conn.sendall('account does not match!'.encode("utf-8"))

                else:
                    conn.sendall('login successfully'.encode("utf-8"))

            elif data["is_login"] == False:
                print("The client ask for register...")
                data.pop("is_login")
                if core.user.register(data["username"], data["password"]):

                    print(data["username"] + ' register successfully!')
                    conn.sendall('register successfully!'.encode("utf-8"))
                else:
                    print("the username already exist! register fail !")
                    conn.sendall('the username already exist! register fail !'.encode("utf-8"))

        elif "close" in data.keys():
            conn.sendall("close successfully!".encode("utf-8"))
            print("close the server!\n")
            conn.close()
            # sk.close()
            exit(0)

        elif "action" in data.keys():
            if data["action"] == "get_player":
                Mylib.server_lib.sendOneToClient(conn, data["username"], data["target"], "send_player")
            elif data["action"] == "get_team":
                Mylib.server_lib.sendOneToClient(conn, data["username"], data["target"], "send_team")
            elif data["action"] == "modify_player" or data["action"] == "modify_team":
                Mylib.server_lib.modifyOne(conn, data["username"], data)
            elif data["action"] == "get_all_player":
                Mylib.server_lib.sendListToClient(conn, data["username"], "send_all_player")
            elif data["action"] == "get_all_team":
                Mylib.server_lib.sendListToClient(conn, data["username"], "send_all_team")
            elif data["action"] == "add_player":
                Mylib.server_lib.addOne(conn, data["username"], "player")
            elif data["action"] == "add_team":
                Mylib.server_lib.addOne(conn, data["username"], "team")
            elif data["action"] == "del_player":
                Mylib.server_lib.delOne(conn, data["username"], "player", data["target"])
            elif data["action"] == "del_team":
                Mylib.server_lib.delOne(conn, data["username"], "team", data["target"])
            elif data["action"] == "add_player_to_team":
                Mylib.server_lib.OpOnePlayerToTeam(conn, data["username"], data)
            elif data["action"] == "del_player_to_team":
                Mylib.server_lib.OpOnePlayerToTeam(conn, data["username"], data)
            elif data["action"] == "start_game":
                Mylib.server_lib.startGame(conn, data["username"], data)
            elif data["action"] == "change_pics":
                Mylib.server_lib.savePics(conn, data["username"], data)
            elif data["action"] == "get_pics":
                Mylib.server_lib.loadPics(conn, data["username"], data["flag"], data["name"])
        conn.close()
        # serSocket.close()
        # self.rfile is a file-like object created by the handler;
        # we can now use e.g. readline() instead of raw recv() calls
        # self.data = self.request.recv(1024)
        # print(self.data)
        # self.request.send(self.data)
        # self.data = self.request.readline().strip()
        # print("{} wrote:".format(self.client_address[0]))
        # print(self.data)
        # # Likewise, self.wfile is a file-like object used to write back
        # # to the client
        # self.request.write(self.data.upper())


host, port = "10.104.173.161", 9999

# with socketserver.TCPServer((host, port), MyTcpHandler) as server:
#     server.serve_forever()
server = socketserver.ThreadingTCPServer((host, 8080), MyTcpHandler)
#server = socketserver.TCPServer((host, port), MyTcpHandler)
#
# sleep = Thread(target=dealwithSleep)
# sleep.start()

sleep = Thread(target = dealwithSleep)
sleep.start()


server.serve_forever()





