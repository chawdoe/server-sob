import pymysql
import core
import json
__username = 'root'#of table
__password = 'ubuntu123'
db_name = 'RUNOOB'

def register(username,password):
    if isUserExist(username):
        return False
    database = pymysql.connect('localhost', __username, __password, db_name)
    cursor = database.cursor()
    cursor.execute('insert into ACCOUNT values("%s", "%s","%s","%s")' % \
                   (username, password,"{}","{}"))
    database.commit()
    database.close()

    return True

def login(username,password):
    database = pymysql.connect('localhost', __username, __password, db_name)
    cursor = database.cursor()
    cursor.execute('SELECT * FROM ACCOUNT WHERE USERNAME= "%s"  AND PASSWORD = "%s" ' %
                   (username, password))
    data_account = cursor.fetchall()
    database.close()
    if data_account:
        return data_account
    else:
        return False

def isUserExist(username):
    database = pymysql.connect('localhost', __username, __password, db_name)
    cursor = database.cursor()
    cursor.execute('SELECT * FROM ACCOUNT WHERE USERNAME= "%s" ' %
                    username )
    data_account = cursor.fetchall()
    database.close()
    if data_account:
        return True
    else:
        return False


def save(dict):##save one user's all player and all name
    sql = 'UPDATE ACCOUNT SET PLAYER = %s WHERE USERNAME = %s'

    if dict["flag"] == "saveplayer":
        db = pymysql.connect('localhost',__username,__password,db_name)
        cursor = db.cursor()
        print(type(dict["content"]))
        cursor.execute("""UPDATE ACCOUNT SET PLAYER = '%s' WHERE USERNAME = '%s' """ %
                      (dict["content"],dict["username"])
                      )
        db.commit()
        db.close()
        return True

    elif dict["flag"] == "saveteam":
        print(type(dict["content"]))
        db = pymysql.connect('localhost',__username,__password,db_name)
        cursor = db.cursor()
        cursor.execute("""UPDATE ACCOUNT SET TEAM = '%s' WHERE USERNAME = '%s' """ %
                      (dict["content"],dict["username"])
                      )
        db.commit()
        db.close()
        return True

def load(dict):#load one user's all teams or all players
    if dict["flag"] == "loadteam":
        db = pymysql.connect('localhost',__username,__password,db_name)
        cursor = db.cursor()
        cursor.execute("""SELECT TEAM FROM ACCOUNT WHERE USERNAME = '%s' """ %
                      dict["username"]
                      )
        data_team = cursor.fetchall()
        db.close()
        return data_team

    elif dict["flag"] == "loadplayer":
        db = pymysql.connect('localhost',__username,__password,db_name)
        cursor = db.cursor()
        cursor.execute("""SELECT PLAYER FROM ACCOUNT WHERE USERNAME = '%s' """ %
                      dict["username"]
                      )
        data_player = cursor.fetchall()
        db.close()
        return data_player

def save_one_user_from_db(user_name):##save the data user who has logged in.
    user_dict = core.user.get_user(user_name)
    user_player_str = json.dumps(user_dict["player"])
    user_team_str = json.dumps(user_dict["team"])

    db = pymysql.connect('localhost', __username, __password, db_name)
    cursor = db.cursor()
    cursor.execute("""UPDATE ACCOUNT SET PLAYER = '%s' WHERE USERNAME = '%s' """ %
                   (user_player_str, user_name)
                   )
    cursor.execute("""UPDATE ACCOUNT SET TEAM = '%s' WHERE USERNAME = '%s' """ %
                   (user_team_str, user_name)
                   )
    db.commit()
    db.close()
    return True

def load_one_user_from_db(user_name):##get the user data who has logged in.
    db = pymysql.connect('localhost', __username, __password, db_name)
    cursor = db.cursor()
    cursor.execute("""SELECT PLAYER FROM ACCOUNT WHERE USERNAME = '%s' """ %
                   user_name
                   )

    str_player = cursor.fetchone()
    print(str_player[0])
    data_player = json.loads(str_player[0])# i do not know why


    cursor.execute("""SELECT TEAM FROM ACCOUNT WHERE USERNAME = '%s' """ %
                   user_name
                   )

    str_team = cursor.fetchone()
    data_team = json.loads(str_team[0])

    core.user.update_user(user_name, data_player, data_team)
    db.close()
