import pymysql

def connectDatabase():
    try:
        database = pymysql.connect('localhost', 'root', 'ubuntu123', 'RUNOOB')
    except OSError:
        return False
    return database

def createAccountTable():
    db = connectDatabase()
    if db!= False:
        cursor = db.cursor()
        sql_createAccountTable = """CREATE TABLE ACCOUNT (
                 USERNAME  CHAR(20) NOT NULL,
                 PASSWORD CHAR(20) NOT NULL ,
                 PLAYER VARCHAR(20000) NULL,
                 TEAM VARCHAR(10000) NULL)"""
        cursor.execute(sql_createAccountTable)
        db.commit()
        db.close()
        return True
    return False

def deleteAccountTable():
    db = connectDatabase()
    if db!= False:
        cursor = db.cursor()
        sql_deleteAccountTable = "DROP TABLE ACCOUNT"
        cursor.execute(sql_deleteAccountTable)
        db.commit()
        db.close()
        return True
    return False

