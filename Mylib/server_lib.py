import core
import Mylib.lib

def sendOneToClient(sk, username, name, flag):#return T/F
    if flag == "send_player":
        info = core.user.get_one_dict(username, "player", name)
    else: # flag == "send_team"
        info = core.user.get_one_dict(username, "team", name)
    try:
        Mylib.lib.sendBig(sk,info)
        return True
    except OSError:
        return False

def sendListToClient(sk, username, flag):#return T/F
    if flag == "send_all_player":
        list_name = core.user.get_all_list(username, "player")
    else:# flag == "send_all_team"
        list_name = core.user.get_all_list(username, "team")

    send_dict = {"flag": flag, "list": list_name}
    try:
        Mylib.lib.sendBig(sk,send_dict)
        return True
    except OSError:
        return False

def modifyOne(sk, username, recv_dict):
    if recv_dict["action"] == "modify_player":##player modify ok!
        try:
            new_name = recv_dict["content"]["name"]
            player_name = recv_dict["target"]#old name

            if player_name != recv_dict["content"]["name"]:##if the name is differ.
                if_successful = core.user.rename_member(username, player_name, new_name)

                if if_successful == False:##if the name is the same as the others.
                    sk.sendall(("please modify the name.").encode("utf-8"))
                    return True

                core.user[username]["player"][new_name].update(recv_dict["content"])
                sk.sendall(("OK").encode("utf-8"))
                return True

            core.user[username]["player"][player_name].update(recv_dict["content"])
            sk.sendall( ("OK").encode("utf-8") )
            return True
        except OSError:
            return False
    else: # flag == "modify_team"
        try:
            team_name = recv_dict["target"]
            new_name = recv_dict["content"]["name"]
            if team_name != recv_dict["content"]["name"]:
                if_successful = core.user.rename_team(username, team_name, new_name)
                if if_successful == False:
                    sk.sendall(("please modify the name.").encode("utf-8"))
                    return True
                core.user[username]["team"][new_name].update(recv_dict["content"])
                sk.sendall(("OK").encode("utf-8"))
                return True
            core.user[username]["team"][team_name].update(recv_dict["content"])
            sk.sendall(("OK").encode("utf-8"))

        except OSError:
            return False

def addOne(sk, username, flag):
    try:
        core.user.add_one(username, flag)
        try:
            Mylib.lib.send(sk, "success!")
        except OSError:
            return False
    except OSError:
        return False

def delOne(sk, username, flag, target):
    oldname = target
    if flag == "player":
        core.user.del_member(username, target)
    else:
        core.user.del_team(username, target)
    try:
        sk.sendall(("OK").encode("utf-8"))
        return True
    except OSError:
        return False

def OpOnePlayerToTeam(sk, username, recv_dict):
    team_name = recv_dict["target_team"]
    player_name = recv_dict["target_player"]

    if recv_dict["action"] == "add_player_to_team":
        if core.user.add_team_member(username, team_name, player_name):
            try:
                sk.sendall(("OK").encode("utf-8"))
                return True
            except OSError:
                return False
    else:
        if core.user.del_team_member(username, team_name, player_name):
            try:
                sk.sendall(("OK").encode("utf-8"))
                return True
            except OSError:
                return False

def startGame(sk, username, recv_dict):
    team_a = recv_dict["team1"]
    team_b = recv_dict["team2"]
    core.user.start_team_game(username, team_a, team_b)
    try:
        sk.sendall(("OK").encode("utf-8"))
        return True
    except OSError:
        return False

def savePics(sk, username, recv_dict):

    print(recv_dict["buffer"])
    path = core.user.set_pics(username, recv_dict["flag"], recv_dict["name"], recv_dict["buffer"])
    if path != "":
        try:
            sk.sendall(("OK").encode("utf-8"))
            return True
        except OSError:
            return False
    else:
        try:
            sk.sendall(("OK").encode("utf-8"))
            return True
        except OSError:
            return False

def loadPics(sk, username, flag, name):
    buffer = core.user.get_pic_buffer(username, flag, name)
    send_dict = {"flag": flag, "name": name, "buffer": buffer}
    if  buffer != None:
        try:
            Mylib.lib.sendBig(sk, send_dict)
            return True
        except OSError:
            return False
    return False