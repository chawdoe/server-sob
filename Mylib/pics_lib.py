import bz2
import base64
from PIL import Image
def picsToB64(pics_path):#retrun str.
    if pics_path == None or pics_path == False or len(pics_path) < 4:
        return None
    img = Image.open(pics_path)
    new_img = img.resize((320, 320), 3)
    img.close()
    buffer = base64.b64encode(bz2.compress(new_img.convert("RGBA").tobytes()))
    new_img.close()
    write_buffer = buffer.decode("ascii")
    return write_buffer

def b64ToPics(b64_buffer, savePathName):
    data = bz2.decompress(base64.b64decode(b64_buffer))
    image = Image.frombytes("RGBA", (320, 320), data)
    image.save(savePathName, "PNG")
    image.close()
    return True